'''py

from datetime import datetime, timezone

from zipline.api import record, symbol, order

''' zipline.api it's where all the commonly used functions of our algorithm are '''
from zipline import run_algorithm

import pandas as pd

''' Panda is a data frame which where the code receives the order of fetching and storing all those collumns, I think '''

def initialize(context):
    pass

''' context is a persistent namespace  for you to store variables you need to need to access from one algorithm 
iteraction to the next

Context it's also an empty dictionary. The dictionary has been augumented so that properties can be 
accessed using the dot notation or the traditional backtest notation '''

# what is an iteration?

# What is an iterable of strings?



def handle_data(context, data):
    order(symbol('AAPL'), 10)
    record(AAPL = data.current(symbol('AAPL'), 'price'))
    order(symbol('MSFT'), 10)
    record(MSFT = data.current(symbol('MSFT'), 'price'))
    
''' In the handle_data function it's where the actually trading happens, which by the way happens 
in the backtest '''


start = datetime(2016, 3, 1, tzinfo = timezone.utc)
end = datetime(2018, 2, 27, tzinfo = timezone.utc)
start = pd.Timestamp(start, tz='UTC', offset = 'C')
end = pd.Timestamp(end, tz='UTC', offset = 'C')

results = run_algorithm(
        start = start,
        end = end,
        initialize = initialize,
        handle_data = handle_data,
        capital_base = 10000,
        data_frequency = 'daily')

'''
